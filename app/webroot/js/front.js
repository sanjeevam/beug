/* Register and Login */
/*when page reloads*/
$(document).ready(function() {
	/*message displayed after sending mail fades away*/
	$('div#passemailmsg').fadeOut(5000);
	/*when error message displays on login page password get disappeared*/
	if(document.webcastLoginForm != undefined) 
	{
		document.webcastLoginForm.WebcastPassword.value="";
	}
	/*when error message displays on register page passwords get disappeared*/
	if(document.webcastRegisterForm != undefined) {
		document.webcastRegisterForm.WebcastPassword.value="";
		document.webcastRegisterForm.WebcastConfirmPassword.value="";
	}
	/*clicking register button*/
	$('#register_formbtn').click(function(){
		$('#login').fadeOut('slow').css('display', 'none');
		$('#forgot_password_form').fadeOut('slow').css('display', 'none');		
		$('#register').fadeIn('slow').css('display', 'block');
		$('#subtitle_header').html('- Registration');
		if(document.webcastLoginForm != undefined) 
		{
			document.webcastLoginForm.WebcastPassword.value="";
			/*removes error messages*/
			document.getElementById('invalidemail_err').style.display = 'none';
			document.getElementById('email_err').style.display = 'none';
			document.getElementById('password_err').style.display = 'none';  
			
		}
	});
	/*clicking login  button*/
	$('#login_formbtn').click(function(){
		$('#register').fadeOut('slow').css('display', 'none');
		$('#forgot_password_form').fadeOut('slow').css('display', 'none');
		$('#login').fadeIn('slow').css('display', 'block');
		
		if(document.webcastRegisterForm != undefined) {
			document.webcastRegisterForm.WebcastPassword.value="";
			document.webcastRegisterForm.WebcastConfirmPassword.value="";
			/*removes error messages*/
			document.getElementById('fname_err').style.display = 'none';
			document.getElementById('lname_err').style.display = 'none';
			document.getElementById('cname_err').style.display = 'none';
			document.getElementById('email1_chkerr').style.display = 'none';
			document.getElementById('email1_err').style.display = 'none';
			document.getElementById('pwd_err').style.display = 'none';
			document.getElementById('cpwd_err').style.display = 'none';
			document.getElementById('checkbox_err').style.display = 'none';
			document.getElementById('cpwd_err_empty').style.display = 'none'; 
			
		}		
		$('#subtitle_header').html('- Login');
	});
	/*after clicking login php error msg disppears*/
	$('#login_btn').click(function(){
		$('div#invalid_user').css('display', 'none');
	});	
	/*after clicking submit php error msg disppears*/
	$('#fpwd_login_btn').click(function(){
		$('div#forgot_pwd').css('display', 'none');
		$('#registeration').fadeOut('slow').css('display', 'none');
	});
	/*after clicking submit php error msg disppears*/
	$('#register_btn').click(function(){
		$('div#already_email').css('display', 'none');
		//$('#subtitle_header').html('- Registration');
	});
	/*after clicking forgot password link*/
	$('#forgot_password').click(function(){
		$('#registeration').fadeOut('slow').css('display', 'none');
		$('#register').fadeOut('slow').css('display', 'none');
		$('#login').fadeOut('slow').css('display', 'none');
		$('#forgot_password_form').fadeIn('slow').css('display', 'block');
		$('#subtitle_header').html('- Forgot Password');
		if(document.webcastForgottenPasswordForm!= undefined) 
		{
			document.webcastForgottenPasswordForm.WebcastEmail.value="";
		}
	});

});
