// Trim function in javascript 
function Trim(str)
{
    while (str.substring(0,1) == ' ') // check for white spaces from beginning
    {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length-1, str.length) == ' ') // check white space from end
    {
        str = str.substring(0,str.length-1);
    }
    return str;
}

//function to validate  login form enteries
function user_validate() {
			  user1 = document.webcastLoginForm.WebcastEmail.value;
			  pass1 = document.webcastLoginForm.WebcastPassword.value;
			  var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
			  var msg = "";
			  if(user1.length == 0) {
				  msg += "Please enter email\n";
                                  document.getElementById('invalidemail_err').style.display = 'none';
				  document.getElementById('email_err').style.display = 'inline';
                                  
			  }
			  else
			  {
				//document.getElementById('email_err').style.display = 'none';
				   if(!re.test(user1))
				  {  
					  msg += "Please enter valid email\n";
                                          document.getElementById('email_err').style.display = 'none';
					  document.getElementById('invalidemail_err').style.display = 'inline';
				  }
                                  else
                                  {   
                                       document.getElementById('invalidemail_err').style.display = 'none';
                                       document.getElementById('email_err').style.display = 'none';
                                  }
			  }
			  if(pass1.length == 0) {
				  msg += "Please enter password\n";
				  document.getElementById('password_err').style.display = 'inline';				  
			  }
              else
              {
                  document.getElementById('password_err').style.display = 'none';                      
              }

			if(msg.length>0) {
				return false;
			 }
			 return true;
 }

 //function to validate  register form enteries
function validate_register() {

	 var f_name = Trim(document.webcastRegisterForm.WebcastFirstName.value);
	 var l_name = Trim(document.webcastRegisterForm.WebcastLastName.value);
	 var c_name = Trim(document.webcastRegisterForm.WebcastCompanyName.value);
	 var email = Trim(document.webcastRegisterForm.WebcastEmail.value);
	 var pwd = document.webcastRegisterForm.WebcastPassword.value;
	 var c_pwd = document.webcastRegisterForm.WebcastConfirmPassword.value;
	 var check_box = document.webcastRegisterForm.WebcastAcknowledge.checked;
	 
	  var msg = "";
	 var e_check = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
	 var p_check = /(?=^.{6,20}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/;
	 
	 if(f_name.length == 0) {
	 
		 msg += "Please enter first name\n";
		 document.getElementById('fname_err').style.display = 'inline';
	 }
     else
     {
         document.getElementById('fname_err').style.display = 'none';                      
     }

	if(l_name.length == 0) {
		  msg += "Please enter last name\n";
		  document.getElementById('lname_err').style.display = 'inline';
	 }
     else
     {
        document.getElementById('lname_err').style.display = 'none';                      
     }

	if(c_name.length == 0) {
		  msg += "Please enter company name\n";
		  document.getElementById('cname_err').style.display = 'inline';
	 }
     else
     {
          document.getElementById('cname_err').style.display = 'none';                      
     }                  
	if(!e_check.test(email)) {
		 msg += "Please enter valid email id\n";
		 document.getElementById('email1_err').style.display = 'inline';
		 document.getElementById('email1_chkerr').style.display = 'none';
	 }
     else
     {
		var chkmail=email.toLowerCase();
		if (chkmail.indexOf("@aol") ==-1 && chkmail.indexOf("@hotmail") ==-1 && chkmail.indexOf("@yahoo") ==-1 && chkmail.indexOf("@gmail") ==-1)
		{
			document.getElementById('email1_err').style.display = 'none';
			document.getElementById('email1_chkerr').style.display = 'none';
		}
		else{
			msg += "Please enter valid email id\n";
			document.getElementById('email1_err').style.display = 'none';
			document.getElementById('email1_chkerr').style.display = 'inline';
		}
     }
		 
	 if (pwd.length == 0) {
	 
		  msg += "Please enter  password\n";
		  document.getElementById('pwd_err').style.display = 'inline';
		 
	 }
	 else
         {
            			 
			if (pwd.length >= 6 && pwd.length <=20){
				
				document.getElementById('pwd_err').style.display = 'none';
				}
			else{
				msg += "Password must be strong\n";
				document.getElementById('pwd_err').style.display = 'inline';
			}
         }  
	
	if(c_pwd.length == 0) {
		  msg += "Please enter confirm password\n";
		  document.getElementById('cpwd_err_empty').style.display = 'inline';
                  document.getElementById('cpwd_err').style.display = 'none';
	 }
    else
    {
            document.getElementById('cpwd_err_empty').style.display = 'none';  
			if(pwd != c_pwd)
			{
				msg += "Password do not match\n";
				document.getElementById('cpwd_err_empty').style.display = 'none';
				document.getElementById('cpwd_err').style.display = 'inline';
			}
			else
			{
				document.getElementById('cpwd_err').style.display = 'none';                      
			}  		
    }  
	
	if(check_box == false)
	{
		
		 msg += "Please tick the checkbox\n";
		 document.getElementById('checkbox_err').style.display = 'inline';
	}
	else
	{
		document.getElementById('checkbox_err').style.display = 'none';                      
	}  	
	 if(msg.length>0) {
		return false;
	}
	return true;
	 
}
//validation on forgot password email
function forgot_pwd_mail()
{
        var fpwd_email = Trim(document.webcastForgottenPasswordForm.WebcastEmail.value);
        
        var msg = "";
        if(fpwd_email.length == 0)
        {
              msg += "Please enter email\n";
              document.getElementById('fpwd_email_err').style.display = 'inline';
        }
        else
        {
              document.getElementById('fpwd_email_err').style.display = 'none';            
        }
        if(msg.length>0)
        {
	    return false;
	}
	return true;
}
