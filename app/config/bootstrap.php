<?php
/* SVN FILE: $Id$ */
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 *
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php is loaded
 * This is an application wide file to load any function that is not used within a class define.
 * You can also use this to include or require any files in your application.
 *
 */
/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * $modelPaths = array('full path to models', 'second full path to models', 'etc...');
 * $viewPaths = array('this path to views', 'second full path to views', 'etc...');
 * $controllerPaths = array('this path to controllers', 'second full path to controllers', 'etc...');
 *
 */ 
/**
 * User Defined Variables
 * @author Mohit Khurana
 */
# Website Title
define('WEBSITE_TITLE', 'BEUG');
# Website Folder
define('WEBSITE_FOLDER', '');
# Site Url
define('SITE_URL', 'http://' . $_SERVER['HTTP_HOST'] .  WEBSITE_FOLDER);
# Absolute Url
define('ABSOLUTE_URL', $_SERVER['DOCUMENT_ROOT']);
# Image Url
define('IMAGE_URL', SITE_URL . DS . "images");
// javascript Url
define('JAVASCRIPT_URL', SITE_URL . DS . "js");
# webcast start time
define('WEBCAST_START_TIME',strtotime('2010-11-09 03:00:00 PM '));
# webcast redirect time in seconds
define('WEBCAST_REDIRECT_TIME',1800);
# webcast date
define('WEBCAST_DATE',strtotime('2010-10-20'));
# webcast line display date and time
define('WEBCAST_DISPLAY_DATETIME',strtotime('2010-11-09 11:59:59 PM'));

/**
 * Mail variables defined here
 *
 */
#SMTP DETAILS
define('SMTP_DETAILS', '');   
#defining host for sending mail
define('SMTP_HOST', '');
#defining port for sending mail
define('SMTP_PORT', '');
#defining from for sending mail
define('MAIL_FROM', 'beug@lexisnexis.co.uk ');
#defining subject sending mail
define('MAIL_SUBJECT', 'BEUG Webcast - Password Reminder');

#defining body for sending mail
define('MAIL_BODY', '<p>Dear BEUG Webcast subscriber,<br/><br/>

Further to your recent request for a password reminder to the BEUG webcast, please find your password below:<br/><br/>

password_new <br/><br/>

You can use this to log-in via the BEUG webcast site at: <a href="http://www.beugwebcast.co.uk/">www.beugwebcast.co.uk</a><br/><br/>

Yours sincerely,<br/><br/>

BEUG Webcast Team</p>');

//EOF
?>