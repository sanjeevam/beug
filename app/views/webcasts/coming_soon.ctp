- Coming Soon!</h1>

</div>
<!--Start Content Section-->
<div id="content">

<h1><strong><?php print $welcome_note; ?> </strong></h1>
<h1>The BEUG Webcast will be displayed on this site on</h1>
<h2><strong>Tuesday 9<sup>th</sup> November 2010 </strong>(3-4pm)</h2>

<p>
Please ensure you have WMP Player installed on your PC in advance of this date. <a href=" http://windows.microsoft.com/en-US/windows/products/windows-media-player" target="_blank">Click here</a> to download if necessary.<br/>
If you wish to test view our webinars, please &nbsp;<a href="http://www.lexisauditorium.co.uk/theatre.aspx?c=11&notest=1" target="_blank">Click here</a>
</p>

   
<p style="font-size:20px; padding-top:0px;">
Should you wish to submit a question to the product experts in advance, please click here: <a href='mailto:beug@lexisnexis.co.uk?subject=<?php print "BEUG Webcast - Question Submission"; ?>' class="btn1" style="text-decoration:none; color:#FFFFFF;">Submit a question</a>
</p>
<p style="padding-top:0px;">
We will endeavor to answer all questions during the live webcast, but if this is not possible we will contact you afterwards.
</p>


<p style="padding-top:0px">
For any BEUG queries contact: <a href="mailto:beug@lexisnexis.co.uk">beug@lexisnexis.co.uk</a>
</p>
<?php echo $form->create('Webcast', array('id'=>'webcastHiddenForm','name'=>'webcastHiddenForm'));?>
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",WEBCAST_START_TIME);?>" name="webcasStartTime">
<input type="hidden" value="<?php echo WEBCAST_REDIRECT_TIME;?>" name="webcastRedirectTime">
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",time());?>" name="webcastServerTime">
<input type="hidden" value="<?php echo $gap;?>" name="webcastDiffTime">
<input type="hidden" value="<?php echo $uname;?>" name="webcastUemail">
<input type="hidden" value="<?php echo $uid;?>" name="webcastUid">
<?php echo $form->end(); ?>
</div>
<!--End Content Section-->
