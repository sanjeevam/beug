</h1>

</div>
<!--Start Content Section-->
<div id="content">

<h1>The latest <strong>BEUG Webcast</strong> is now ready</h1>
<h1>to view, if you click here:</h1>
<div><input type="button" value="Watch Now" class="btn" onclick= "window.open('http://lexisauditorium.com/theatre.aspx?c=779&fwd=1')" /></div>



<p style="font-size:20px; padding-top:0px">
Once you have viewed the webcast, we would be most grateful if you could take a few minutes to complete this short 3 questions survey:<br />
<br />

<input type="button" value="Survey Now" class="btn"  onclick= "window.open('http://www.surveymonkey.com/s/NLG2Y7G')"/>
</p>

<p><strong>BEUG</strong> keeps you informed on what content and functionality is coming soon on Lexis&reg;Library and is delivered by LexisNexis product experts.
<?php
	
		if($display_time<=0)
		{?>
			This webcast was recorded on 9<sup>th</sup>NOV 2010
		<?php } ?>
</p>
<?php echo $form->create('Webcast', array('id'=>'webcastHiddenForm','name'=>'webcastHiddenForm'));?>
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",WEBCAST_START_TIME);?>" name="webcasStartTime">
<input type="hidden" value="<?php echo WEBCAST_REDIRECT_TIME;?>" name="webcastRedirectTime">
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",time());?>" name="webcastServerTime">
<input type="hidden" value="<?php echo $gap;?>" name="webcastDiffTime">
<input type="hidden" value="<?php echo $uname;?>" name="webcastUemail">
<input type="hidden" value="<?php echo $uid;?>" name="webcastUid">
<?php echo $form->end(); ?>




</div>
<!--End Content Section-->