<span id="subtitle_header"><?php print $subtitle_header; ?></span></h1>

</div>
<!--Start Content Section-->
<div id="content">
	<?php $form3style ="";
	if(isset($error_mail) && $error_mail == 'invalid') {
		$form3style = 'style="display:none;"';
		
	}
	?>
<div id="registeration" <?php print $form3style;?>>
<h1>Register below for your free dedicated access to the <strong>BEUG Webcast</strong> on :</h1>
<h2><strong>Tuesday 9<sup>th</sup> November 2010 </strong>(3-4pm)</h2>

<div>
<input type="button" name="register" id="register_formbtn" value="Register" class="btn"  />
</div>
<?php
	/*if(isset($mailsent) && trim($mailsent) != ''){
		print '<br /><br/><div id="already_email" class="registration_error">'.$mailsent.'</div>';
	}*/
?>
<!--<input type="button" name="login" id="login_formbtn" value="Login" class="btn"/>-->
<?php
	
	$form1style = $form2style = $emailexists = "";
	$form_forgotpass_style = "style='display:none;'";
	//Seeting blank values.
	$pwd_email = $fpwd_mail = "";
	if(isset($show_msg) && $show_msg == 'invalid'){ 
		$form1style = 'style="display:block;"';
		$emailexists = '<div id="already_email" class="registration_error">User is already registered with the same email id</div>';
		$form2style = 'style="display:none;"';
		$form_forgotpass_style = 'style="display:none;"';
		
	}
	if(isset($error_mail) && $error_mail == 'invalid') {
	//$Session->check('error_mail');
	
		//print $error_mail;
		$form1style = 'style="display:none;"';
		$form2style = 'style="display:none;"';
		$form3style = 'style="display:none;"';
		$form_forgotpass_style = 'style="display:block;"';
		$pwd_email  = '<div id="forgot_pwd" class="registration_error">Entered email id does not exist in the system</div>';
	}
	if(isset($mailsent) && trim($mailsent) != ''){
		$form1style = 'style="display:none;"';
		$form2style = 'style="display:block;"';
		$form_forgotpass_style = 'style="display:none;"';
		//$fpwd_mail = '<div id="passemailmsg" class="registration_error">'.$mailsent.'</div>';
		$fpwd_mail = $mailsent;
		
	}
	?>


</div>

<?php echo $form->create('Webcast', array('action'=>'/index/register','id'=>'webcastRegisterForm','name'=>'webcastRegisterForm','onSubmit'=>'return validate_register()'));?>
<div class="form" id="register" <?php print $form1style; ?>>
	<?php print $emailexists; ?>

	<ul>
    	<li>
			<label>First Name</label>
				<?php echo $form->input('first_name',array('label'=>false,'class'=>'textbox','maxlength'=>'40','div'=>false));?>
			<span id="fname_err" class="error">Please enter first name</span>
		</li>
        <li>
			<label>Last Name</label>
				<?php echo $form->input('last_name',array('label'=>false,'class'=>'textbox','maxlength'=>'40','div'=>false));?>
			<span id="lname_err" class="error">Please enter last name</span>
		</li>
        <li>
			<label>Company Name</label>
				<?php echo $form->input('company_name',array('label'=>false,'class'=>'textbox','maxlength'=>'45','div'=>false));?>
			<span id="cname_err" class="error">Please enter company name</span>
		</li>
        <li>
			<label>Email</label>
			<?php echo $form->input('email',array('label'=>false,'class'=>'textbox','maxlength'=>'75','div'=>false, 'value' =>$email_value, 'onfocus' => 'javascript:if(this.value=="Enter Work email only")this.value="";', 'onblur' => 'javascript:if(this.value=="")this.value="Enter Work email only";'));?>
			<span id="email1_err" class="error">Please enter email id</span>
			<span id="email1_chkerr" class="error">Please enter business email id only</span>
		</li>
        <li>
			<label>Password</label>
				<?php echo $form->input('password',array('label'=>false, 'type' => 'password', 'class'=>'textbox password','maxlength'=>'20','div'=>false));?>
			<span id="pwd_err" class="error">Please enter password. Length should be 6-20 characters.</span>
			<!--<span id="pwdlen_err" class="error" border="1"> Password should include at least one number and both upper and lowercase alphabet</span>-->
		</li>
        <li>
			<label>Confirm Password</label>
				<?php echo $form->input('confirm_password',array('label'=>false, 'type' => 'password', 'class'=>'textbox password','maxlength'=>'20','div'=>false));?>
			<span id="cpwd_err_empty" class="error">Please enter confirm password</span>
			<span id="cpwd_err" class="error">Passwords do not match</span>
		</li>
		<li>
		<table width="960" border="0" cellpadding="3" cellspacing="0">
			  <tr>
				
				<td style="text-align:right; width:311px; vertical-align:top"><img src="<?php print SITE_URL; ?>/images/lock.jpg" width="27" height="39" alt="" /></td>
				<td style="text-align:right; width:17px;"><?php echo $form->checkbox('info_share',array('label'=>false));?></td>
				<td><span style="color:#333; padding-left:0px; line-height:14px">
			If you do NOT wish to be kept informed about our BEUG webcasts by E-mail, please tick this box.  </span></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td valign="top"><?php echo $form->checkbox('acknowledge',array('label'=>false));?></td>
				<td><span style="color:#333; padding-left:0px; line-height:14px">
					By ticking this box  I acknowledge that I have read and agree to abide by the LexisNexis UK
					 <a style="3d; font-size:12px" href="http://www.lexisnexis.co.uk/privacy/" target="_blank">privacy policy</a>.</span></td>
								
			  </tr>			  
			  <tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td><span id="checkbox_err" class="error">"I acknowledge" box must be ticked before the user can register</span>
					 </td></tr>
			  
		</table>
			
			
		  
		</li>
		

        <li style="text-align:center">
		  	<input type="submit" name="login" value="Submit" class="btn" id="register_btn" />
		</li>
    </ul>
</div>
<?php echo $form->end(); ?>
<?php echo $form->create('Webcast', array('action'=>'/index/login','id'=>'webcastLoginForm','name'=>'webcastLoginForm','onSubmit'=>'return user_validate()'));?>
<div class="form" id="login" <?php print $form2style ; ?>>
	<?php if(isset($fpwd_mail) && trim($fpwd_mail) != ""){ ?>
		<div id="passemailmsg" class="registration_error"><?php print $fpwd_mail ;?></div>
	<?php } ?>
	<p style="line-height:0px;">If you have already registered your details, please log in here:</p>
	<?php
		if(isset($errorlogin) && $errorlogin == 'invalid'){ ?>
		<div id="invalid_user" class="registration_error">Invalid username or password</div>
	<?php  }?>
	<ul>
    	<li>
			<label>Email</label>
			<?php echo $form->input('email',array('label'=>false,'class'=>'textbox','maxlength'=>'75','div'=>false));?>
			<span id="email_err" class="error">Please enter email id</span>
			<span id="invalidemail_err" class="error">Please enter valid email id</span>
		</li>
        <li>
			<label>Password</label>
			<?php echo $form->input('password',array('label'=>false, 'type' => 'password', 'class'=>'textbox password','maxlength'=>'20','div'=>false));?>
			<span id="password_err" class="error">Please enter password</span>
			<!-- for forgotten password-->
			<p>
			If you have forgotten your password please <a href="#" id="forgot_password">Click here</a>
			</p>
			<!-- for forgotten password-->

		</li>
        <li style="text-align:center"><input type="submit" name="login" value="login" class="btn"  id="login_btn" /></li>
    </ul>
	
	
</div>
<?php echo $form->end(); ?>
<?php echo $form->create('Webcast', array('action'=>'/forgot_password','id'=>'webcastForgottenPasswordForm','name'=>'webcastForgottenPasswordForm','onSubmit'=>'return forgot_pwd_mail()'));?>
<div class="form" id="forgot_password_form" <?php print $form_forgotpass_style; ?>>
	<h1 >Forgot Password</h1>
    <?php 
	print $pwd_email;
	//print $fpwd_mail;
	
	?>	

	<ul>
    	<li>
			<label>Email</label>
			<?php echo $form->input('email',array('label'=>false,'class'=>'textbox','maxlength'=>'75','div'=>false));?>
			<span id="fpwd_email_err" class="error">Please enter email id</span>
			
		</li>
        <li style="text-align:center"><input type="submit" name="login" value="Submit" class="btn"  id="fpwd_login_btn" /></li>
    </ul>
</div>
<?php echo $form->end(); ?>
<?php echo $form->create('Webcast', array('id'=>'webcastHiddenForm','name'=>'webcastHiddenForm'));?>
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",WEBCAST_START_TIME);?>" name="webcasStartTime">
<input type="hidden" value="<?php echo WEBCAST_REDIRECT_TIME;?>" name="webcastRedirectTime">
<input type="hidden" value="<?php echo strftime("%d,%m,%Y,%I,%M,%S,%p",time());?>" name="webcastServerTime">
<?php echo $form->end(); ?>
<p>
BEUG is a preview of new content and functionality developments on Lexis&reg;Library. It gives you advanced warning of what's coming soon on the service and is delivered by Lexis Nexis product experts. For any BEUG queries contact: <a style="3d" href="mailto:beug@lexisnexis.co.uk">beug@lexisnexis.co.uk</a>
</p>

</div>
<!--End Content Section-->
