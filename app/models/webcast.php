<?php

class Webcast extends AppModel
{
	var $name = 'Webcast';
	var $useTable = 'users';
	#check whether the logged in user is registered or not
	function check_login($email_id,$password )
	{
	#counts the records similar to the entered email_id and password
			$check_login_data = $this->find('count',array(
				'conditions'=>array(
					'Webcast.email ' =>$email_id ,
					'Webcast.password' => $password 
					)
				)
			);	
			return $check_login_data;
	}
	#getting email id
	function to_register_id($email_id )
    {
        $register_id = $this->find('first',array(
				'conditions'=>array(
					'Webcast.email ' =>$email_id 
					)
				)
			);	
			return $register_id ;
    }

	function check_email($email_id )
	{
	#counts the records similar to the entered email_id
			$check_email_data = $this->find('count',array(
				'conditions'=>array(
					'Webcast.email ' =>$email_id ,
					
					)
				)
			);	
			return $check_email_data;
	}
	
	function fetch_uname($user_id )
    {
        $uname = $this->find('first',array(
				'conditions'=>array(
					'Webcast.id ' =>$user_id
					)
				)
			);	
			return $uname;
    }
}
?>