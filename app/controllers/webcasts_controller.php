<?php
/**
 * This is the master webcasts class that will handle all the master pages for webcast requests.
 * @author Sanchali Bishnoi
 */
class WebcastsController extends AppController 
{

    #Controller Class Name
    var $name = 'Webcasts';
    # Array of helpers used in this controller.
    var $helpers = array('Html','Javascript','Ajax','Form');
    # Session component to handle the sessions.
    var $components  = array('Session','Email');

    /**
    *function for login and registration 
    */
    function index($action = '', $param2 = '') 
    {   
	    $this->layout = 'front';
	#displays email value in the textbox
	    $this->set('email_value', 'Enter Work email only');
	#setting the title
	    $this->set('sub_title', '- Login / Registration');
	#displays login at the top on home page
	    $subtitle_header = "- Login";
	#to register user details
    	if($action == 'register')
	    {
		#when data is recieved
		   if(isset($this->data) && !empty($this->data) )
		    {	
			    $record_data = $this->data;
			    $email_id = $record_data['Webcast']['email'];
			    # check model with the email id entered
			    App::import('Model', 'Webcast');
			    $this->Webcast = new Webcast();
			    $arr_count_email = $this->Webcast->check_email($email_id);
			    #if email id is not present in the database
			    if($arr_count_email == 0)
			    {
				    #saving encrypted password
				    $record_data['Webcast']['password'] = md5($record_data['Webcast']['password']);
				    #saved data in database
					if(intval($record_data['Webcast']['info_share']) == 1)
						$record_data['Webcast']['info_share'] = 'NO';
					else	
						$record_data['Webcast']['info_share'] = 'YES';
				    #saving data
				    $this->Webcast->save($record_data['Webcast']);
				    #fetching id from the database
				    $arr_register_id = $this->Webcast->to_register_id($email_id);
				    $user_id = $arr_register_id['Webcast']['id'];
				   
				    #adding sessions
				    $this->Session->write('current_user_name',$email_id);
				    $this->Session->write('current_user_id',$user_id);
				    
				     #inserting login details in the table
				    App::import('Model', 'LoginDetail');
				    $this->LoginDetail = new LoginDetail();
				    $login['LoginDetail']['user_id'] = $user_id ;
				    $this->LoginDetail->save( $login['LoginDetail']);
				    //pr( $login['LoginDetail']);die;
				    #redirecting to third page
				    
				    $this->redirect(SITE_URL.'/webcasts/castweb');
			    }
			    else
			    {
				#if email id present in the databse,shows error message
				    $this->set('show_msg','invalid');
				#email id value stays in the textbox after pressing submit    
				    $this->set('email_value', $record_data['Webcast']['email']);
			    }
		    }
		    #setting registeratin header
		    $subtitle_header = "- Registration";
	    }
     	elseif($action == 'login')
	    {
		
		    # Redirect on 3rd page if a user logs in before 30 minutes of the webinar.
		    # else redirect to 2nd page in usual.
		    	# recieved data after pressing login button
			    if(isset($this->data) && !empty($this->data) )
			    {
				    $email_id = $this->data['Webcast']['email'];
				    $password = md5($this->data['Webcast']['password']);
				    #check database the records matching the entered password and email id
				    App::import('Model', 'Webcast');
				    $this->Webcast = new Webcast();
				    $arr_count_data = $this->Webcast->check_login($email_id,$password);
				    
				    # If the entered email_id and passwords present in the database redirected to second page.
				    if($arr_count_data >0)
				    {	
					    $arr_register_id = $this->Webcast->to_register_id($email_id);
					    #fetching user id
					    $user_id = $arr_register_id['Webcast']['id'];
					   
					    #adding sessions
					    $this->Session->write('current_user_name',$email_id);
					    $this->Session->write('current_user_id',$user_id);
					    
					     #inserting login details in the table
					    App::import('Model', 'LoginDetail');
					    $this->LoginDetail = new LoginDetail();
					    $login['LoginDetail']['user_id'] = $user_id ;
					    $this->LoginDetail->save( $login['LoginDetail']);
					    //pr( $this->LoginDetail);die;
					    
					    #redirecting to 3rd page
					    $this->redirect(SITE_URL.'/webcasts/castweb');
					
				    }
				    else
				    {
					#if username or password invalid,shows error message
					    $this->set('errorlogin', 'invalid');
				    }
			    }
			#setting the header 
			$subtitle_header = "- Login";    
		   
	    }
          elseif($action == 'forgot_password')	    
	  {$subtitle_header = "- Forgot Password";
	    if($param2 == 'mailsent')
	    {
		#shows message that mail has been sent
		$this->set('mailsent', 'A new password has been sent to your email address');
		$subtitle_header = "- login";
	    }else
	    {
		#setting error message if email id does not exist in the database
		$this->set('error_mail', 'invalid');
	    }
	    #setting the forgot password header
	    
	  }
	  #transferring to view
	  $this->set('subtitle_header', $subtitle_header);    
    
    }
    
    /**
    *function to start the webinar
    */

    function castweb() 
    {
		$this->layout = 'front';
		#setting the title
		$this->set('sub_title', ' - Webcast');
		#call function to check if the user is logged in
		$this->check_user_loggedin();
		#checking time interval before webinar
		$gap = intval(WEBCAST_START_TIME-time());
		#to check problem
		$this->set('gap',$gap);
		$uname = $this->Session->read('current_user_name');
		$this->set('uname',$uname);
		$uid = $this->Session->read('current_user_id');
		$this->set('uid',$uid);
		# Redirect on 2nd page if a user logs in before 30 minutes of the webinar.
		if($gap>WEBCAST_REDIRECT_TIME) {
			$this->redirect(SITE_URL.'/webcasts/coming_soon');
		}
		#checking time interval before webinar
		$display_time = intval(WEBCAST_DISPLAY_DATETIME-time());
		#passing interval to the page
		$this->set('display_time', $display_time);
    }
    /**
    *function to show what's new in lexis nexis
    */
    function coming_soon() 
    {
		$this->layout = 'front';
		#setting the title
		$this->set('sub_title', 'Webcast - Coming soon');
		#call function to check if the user is logged in		
		$this->check_user_loggedin();

	    # Set Webcast Controller.
	    App::import('Model', 'Webcast');
	    # Create Contenttype model object.
	    $this->Webcast = new Webcast();	
		#fetching data from the database reading session 
		$user = $this->Webcast->findById($this->Session->read('current_user_id'));
		#setting firstname of the user at the top if entered else shows hardcored message
		if(trim($user['Webcast']['first_name']) != "")
			$this->set('welcome_note', 'Welcome ' . $user['Webcast']['first_name'] . ' !');
		else 
			$this->set('welcome_note', 'Welcome Guest !');		
		#checking time interval before webinar	
		$gap = intval(WEBCAST_START_TIME-time());
		#to check problem
		$this->set('gap',$gap);
		$uname = $this->Session->read('current_user_name');
		$this->set('uname',$uname);
		$uid = $this->Session->read('current_user_id');
		$this->set('uid',$uid);
		# Redirect on 3rd page if a user logs in before 30 minutes of the webinar.
		if($gap<=WEBCAST_REDIRECT_TIME) {
			$this->redirect(SITE_URL.'/webcasts/castweb');
		}
		
    }
    /*
    *function to unset the session 
    */
    function unsetuser()
    {
		$this->Session->delete('current_user_name');
		$this->Session->delete('current_user_id');
		$this->redirect(SITE_URL."/");
    }
    
    # Export user details to excel.
    function excelExport() 
    {
	    $this->layout = false;
	    # Set Webcast Controller.
	    App::import('Model', 'Webcast');
	    # Create Contenttype model object.
	    $this->Webcast = new Webcast();		
	    $result = $this->Webcast->find('all', array('conditions'=>1));
	   
		# Data header in excel sheet.
		$arrExcelInfo[0] = array('User Id', 'First Name', 'Last Name', 'Company', 'E-mail', 'Share Information', 'Created Date');

		# Content Array.
		$arrExcelInfo[1] = array();
		# Import of vendor excel.
		App::import('Vendor', 'excelVendor', array('file' =>'excel'.DS.'class.export_excel.php'));
		# File name for excel.	
		$fileName = "user_export.xls";
		# Create the instance of the exportexcel format.
		$excel_obj = new ExportExcel("$fileName");
		$i = 0;
		if(is_array($result) && !empty($result)){
			foreach ($result as $keySet => $valueSet)
			{
				# Mapping common fields.
				$arrExcelInfo[1][$i][0] = $valueSet['Webcast']['id'];
				$arrExcelInfo[1][$i][1] = $valueSet['Webcast']['first_name'];
				$arrExcelInfo[1][$i][2] = $valueSet['Webcast']['last_name'];
				$arrExcelInfo[1][$i][3] = $valueSet['Webcast']['company_name'];
				$arrExcelInfo[1][$i][4] = $valueSet['Webcast']['email'];
				$arrExcelInfo[1][$i][5] = ucfirst(strtolower($valueSet['Webcast']['info_share']));
				$arrExcelInfo[1][$i][6] = date('dS M, Y', strtotime($valueSet['Webcast']['created']));
				$i++;
			}
		}else{
			$arrExcelInfo[1][$i][0] = "No record found";		
		}

	    /**
	    * Setting the values of the headers and data of the excel file 
	    * and these values comes from the other file which file shows the data
	    */
	    $excel_obj->setHeadersAndValues($arrExcelInfo[0],$arrExcelInfo[1]); 
	    
	    # Now generate the excel file with the data and headers set.
	    $excel_obj->GenerateExcelFile();
    }
    
     # Export login details to excel.
    function loginExcelExport() 
    {
	    $this->layout = false;
	    # Set Webcast Controller.
	    App::import('Model', 'LoginDetail');
	    # Create Content type model object.
	    $this->LoginDetail = new LoginDetail();
	    #fetching all records of the table
	    $result1 = $this->LoginDetail->find('all', array('conditions'=>1,'order'=>array('LoginDetail.login_time DESC','LoginDetail.id DESC')));
	     
			# Data header in excel sheet.
			$arrExcelInfo[0] = array('User Id', 'First Name', 'Last Name', 'Company Name', 'E-Mail', 'Login Time');
			
			# Content Array.
			$arrExcelInfo[1] = array();
			# Import of vendor excel.
			App::import('Vendor', 'excelVendor', array('file' =>'excel'.DS.'class.export_excel.php'));
			# File name for excel.	
			$fileName = "login_export.xls";
			# Create the instance of the exportexcel format.
			$login_excel_obj = new ExportExcel("$fileName");
			$i = 0;
			if(is_array($result1) && !empty($result1)){
					# Import model webcast
					App::import('Model', 'Webcast');
					# Object of model webcast
					$this->Webcast = new Webcast();

				foreach ($result1 as $keySet => $valueSet)
				{
					# Mapping common fields.
					$arrExcelInfo[1][$i][0] = $valueSet['LoginDetail']['user_id'];
					$arr_uname = $this->Webcast-> fetch_uname($arrExcelInfo[1][$i][0] );
					$arrExcelInfo[1][$i][1] =$arr_uname['Webcast']['first_name'];
					$arrExcelInfo[1][$i][2] =$arr_uname['Webcast']['last_name'];
					$arrExcelInfo[1][$i][3] =$arr_uname['Webcast']['company_name'];
					$arrExcelInfo[1][$i][4] =$arr_uname['Webcast']['email'];
					$arrExcelInfo[1][$i][5] = date('dS M, Y h:i:s a', strtotime($valueSet['LoginDetail']['login_time']));
					$i++;
				}
		}else{
				$arrExcelInfo[1][$i][0] = "No record found";		
		}

				/**
				* Setting the values of the headers and data of the excel file 
				* and these values comes from the other file which file shows the data
				*/
				$login_excel_obj ->setHeadersAndValues($arrExcelInfo[0],$arrExcelInfo[1]); 
				
				# Now generate the excel file with the data and headers set.
				$login_excel_obj->GenerateExcelFile();
    }
    /*
     *function to check if the user has logged in or not
     */
    function check_user_loggedin()
    {
		if($this->Session->read('current_user_id')<1)
		{
		   #if not logged in tranferred to the home page
		   $this->redirect(SITE_URL.'/');
		}
    }
    /*
    *to recover forgotten password	
    */
    function forgot_password()
    {
	#recieved data
	if(isset($this->data) && !empty($this->data) )
	{
	    #storing email id in a variable
	    $email_for_pwd = $this->data['Webcast']['email'];
	    
	    #checking whether entered email id exists
	    App::import('Model', 'Webcast');
	    $this->Webcast = new Webcast();
	    #checking if email id exists in the database
	    $arr_count = $this->Webcast->check_email($email_for_pwd);
	    #if email id is found the status is 1 else 0
	    if($arr_count)
	    {
		$status = 1;
	    }
	    else
	    {
		$status = 0;
		$action= "forgot_password";
		#redirecting to the home page when email id doesnot exist
		$this->redirect(SITE_URL.'/webcasts/index/'.$action);
	    }
	    #sending mail and restting password in DB if email found in the database
	    if($status)
	    {
		#calling random password function to generate a random password
		$rand_pwd = $this->generate_random_password();
		#encrypting the password
		$rand_md5_pwd = md5($rand_pwd);
		App::import('Model', 'Webcast');
		$this->Webcast = new Webcast();
		#updating the records bu entering encrypted password in the database checking condition by email id
		$this->Webcast->updateAll(array('Webcast.password' =>"'".$rand_md5_pwd ."'"),array('Webcast.email'=>$email_for_pwd));
		# caling sending mail function
		$this->send_mail($rand_pwd,$email_for_pwd);
	    }	
	    
	}
    }
    /*
    *generating random password
    */
    function generate_random_password()
    {
	#array from which the characters will be taken
	$salt = "abchefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789";
	#process that will be used by the rand()function to generate random numbers
	srand((double)microtime()*1000000);

	$i = 0;
	$pass="";
	#will generate password of length 8
	    while ($i <= 7)
	    {
		$num = intval(rand() % 33);
		$tmp = substr($salt, $num, 1);
		$pass = $pass . $tmp;
		$i++;
	    }
    
	return $pass;
    }
    /*
    *sending mail
    */
    function send_mail($rand_pwd, $email_for_pwd)
    {
	
	if(SMTP_HOST != '')
	{
	   $this->Email->smtpOptions = array(
	    'port'=>SMTP_PORT,
	    'host' =>SMTP_HOST,
	    );
	}
	$this->Email->to = $email_for_pwd;
	#subject field of the mail
	$this->Email->subject = trim(MAIL_SUBJECT);
	#from field of the mail
	$this->Email->from = trim(MAIL_FROM);
	
	#body field of the mail 
	$msg_body = MAIL_BODY;
	#replacing !password_new with the random password
	$msg_body = str_replace("password_new",$rand_pwd ,$msg_body);
			
	#$this->Email->fromName = 'BEUG';
	#setting this for elements/email/html
	$this->set('msg_body',$msg_body);
	#sending type
	$this->Email->sendAs = 'html';
	#for layouts/email/default.ctp	
	$this->Email->template = 'password_mail';
	#sending mail function					
		if ($this->Email->send())
		{
		    $this->redirect(SITE_URL."/webcasts/index/forgot_password/mailsent");
		}
	    
    }
    
    
    
 }
?>
